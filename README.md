# spring-cloud-serverless-config

This repo contains a Dockerfile and associated Kubernetes manifest to support the spring-cloud-serverless service example.  These artifacts serve as a basic example of multi-stage docker file which builds and produces a minimal runtime image along with a basic Kubernetes deployment.

# Artifacts

* Dockerfile
  * Multi-stage dockerfile which clones the source and packages an executable jar.
  * Build tools are isolated to the build stage.  Only the built artifact is copied to the run image.
  * The run image is a slim debian image with minimal dependencies. (could be more minimal possibly).
  * A user is created with limited file system access.
  * The listening port is exposed.
  * The jar is owned by root and cannot be modified witin the container.

* Manifest
  * A kubernetes deployment with service, horizontal pod autoscaler, and ingress object.
  * Deployment is configured to run a single container.
  * There is an emptyDir volume for Tomcat's logs to allow for readOnlyFilesystem. (may be able to eliminate this, but do not have time to dig into app).
  * Liveness/Readiness probes are configured per spring boot conventions.
  * A horizontal pod autoscaler is configured based on cpu utilization. 
  * An ingress resources is defined and works on my local kind cluster.  (not fully tested with a LB, but conformant).
  * Ingress is annotated to generate a TLS certificate from letsencrypt-staging for host. (based on a local certmanager deployment).


# Conclusion

This is a basic deployment of a stateless app.  In real life there would likely be the need to consider secrets, observabilty, configuration management, and manifest standards.

Some future considerations:
- Establish an approach for continious integration (scanning Dockerfile, image, manifests for conformance) and Continious Delivery (Argo, Flux, etc).
- Move to a standard library like helm or jsonnet to generate kubernetes manifests bundling standard conventions and exposing configurations (hardcoded manifests won't scale).
- Standardize docker build, running against locally vetted base images, use CI/CD to enforce image updates.
- Determine appropriate way to include environmental configuration.  Potentially accomplished through CICD, namespace/cluster provisions, etc. (this example is very stateless so maybe none?).
- Integrate with a secrets solutions vault, external secrets operator, or sealed secrets.
- Begin collecting metrics, aggregating logs with a cluster level solution (Prometheus, Loki, etc.).
  
# Running the Example

Build a container based on your dockerfile by running this command from the root of the project. Modify the image tag in accordance with your selected container registry.  (Note you may need to authenticate to your container registry by running `docker login`)

```bash
  #tseufert1 is my dockehub registry
  $ docker buildx build -t tseufert1/hello-app:0.0.1 . --push  
```

Set your ingress host in the manifest.yaml file.

```yaml
  apiVersion: networking.k8s.io/v1
  kind: Ingress
  metadata:
    name: hello-fun-app-service
    namespace: hello-fun-app
  spec:
    rules:
    #set the host value based on the dns configured for your ingress controller's loadbalancer 
    #*.tsfrt.net is the domain name I have associated with my ingress controller
    #hello-fun is the subdomain I am defining so the ingress controller routes to my configured service
    - host: hello-fun.tsfrt.net 
      http:
        paths:
        - pathType: Prefix
          path: /
          backend:
            service:
              name: hello-fun-app-service
              port:
                number: 8080
```

Set the image reference in the deployment located in your manifest.yaml based on the docker build/tag command run above.

```yaml
  #tseufert1/hello-app:0.0.1 was the value I used in my docker build command
  image: tseufert1/hello-app:0.0.1 #set the image registry based on your built/tagged image

```

Apply the kubernetes config.

```bash
   #this command invocation assumes you are located in the directory of the manifest.yaml file we modified in the last 2 steps
   $ kubectl apply -f manifest.yaml

```
