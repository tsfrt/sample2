ARG BUILD_VERSION=0.0.1-SNAPSHOT
ARG APP_NAME=hello-fun
ARG APP_PATH=/build/application-accelerator-samples/spring-cloud-serverless

FROM maven:3.8.7-openjdk-18 as builder

ARG BUILD_VERSION
ARG APP_NAME
ARG APP_PATH

WORKDIR /build

RUN git clone https://github.com/vmware-tanzu/application-accelerator-samples.git && \
    cd ${APP_PATH} && \
    mvn package

FROM openjdk:22-slim-bullseye

ARG BUILD_VERSION
ARG APP_NAME
ARG APP_PATH

RUN groupadd -r app-runner \
    && useradd -r -s /bin/false -g app-runner app-runner 

WORKDIR /app
RUN ls .
COPY --from=builder ${APP_PATH}/target/${APP_NAME}-${BUILD_VERSION}.jar ./

RUN chmod +x ${APP_NAME}-${BUILD_VERSION}.jar
USER app-runner

EXPOSE 8080

ENV BUILD_VERSION=${BUILD_VERSION}
ENV APP_NAME=${APP_NAME}

ENTRYPOINT ["sh", "-c", "java -jar ${APP_NAME}-${BUILD_VERSION}.jar"]



